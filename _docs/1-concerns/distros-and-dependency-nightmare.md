---
title: "Distros & Dependency Nightmare"
category: Concerns
order: 3
---
**"Managing dependencies for Linux is a nightmare because there are many distributions and they each have different libraries."**

The issue of how to manage the dependencies of your software on Linux is something that catches many developers off guard and can create headaches in supporting Linux. There are a number of ways you can deal with this challenge that greatly simplify the problem, reducing the complexity considerably down to the trivial matter of targeting a single runtime.

**Kernel Versions**

With Linux distributions often updating their kernel version, [it's sometimes an expressed concern or question,](https://twitter.com/olafurw/status/1155560620433035264?s=19) "What kernel version should my game target?".

The answer is: You don't need to worry about Linux kernel versions.

The first reason why, is because most programs do not depend or interact directly with kernel interfaces.

The second reason why, is Linux kernel development operates under a strict mantra of: "We do not break userspace".

[In the words of Linus Torvalds himself:](https://yarchive.net/comp/linux/gcc_vs_kernel_stability.html)

> We care about user-space interfaces to an insane degree. We go to extreme lengths to maintain even badly designed or unintentional interfaces. Breaking user programs simply isn't acceptable. (…) We know that people use old binaries for years and years, and that making a new release doesn't mean that you can just throw that out. You can trust us.

Newer Linux kernel versions may expose new interfaces, but old interfaces remain supported.

**Managing Dependencies**

Ethan Lee, a professional Linux porter, [wrote an excellent write up](https://gist.github.com/flibitijibibo/b67910842ab95bb3decdf89d1502de88) with advice on managing the complexity of packaging games for Linux. It is well worth a read for advice from an experienced porter on how to best handle the task.

**Take advantage of the Steam Runtime**

Steam for Linux ships with its own set of libraries called the [Steam Runtime](https://github.com/ValveSoftware/steam-runtime). By default Steam launches all Steam Applications within the runtime environment. The Steam Runtime is based off Ubuntu 12.04. More information about this runtime [can be obtained here.](https://partner.steamgames.com/doc/store/application/platforms/linux) By targeting this runtime, you can ensure your application runs with a consist available set of libraries on all platforms. Any dependencies that you require that are not available in the Steam Runtime, you should package as .so files with your game.

**Use AppImage or Flatpak**

[AppImage](https://appimage.org/) and [Flatpak](https://flatpak.org/) are container formats for distributing software on Linux and allow for packaging an entire runtime with them, so an application can bring with it all of it's own dependencies, to ensure the application works exactly as expected across all Linux distributions that support AppImage and Flatpak. Consider packaging your game into one of these formats

If you are not sure which format to go with, this author recommends AppImage for it's simplicity.

**Take advantage of existing game engines & middleware that strongly support Linux**

This is less of an option if you already have a game developed, but if you are still in the planning stage for a game, you will find it much easier to support Linux by choosing technologies that are known to easily support multiply platforms including Linux, such as Unreal Engine, Unity, Godot, SDL, and so on. Read the related 'Recommended Middleware' section for further advice.

**Target a specific Linux Distribution**

If you are still worried about potential issues over library dependencies and compatibility issues of running your game across multiple Linux distros, or the burden of support, there's another alternative, simply state in the requirements for your game that you only officially support one Linux distribution. The current popular choice is the most popular Linux distribution on Steam, Ubuntu.

One of the best things about Linux is the freedom to use it how you like, to customise Linux and go nuts with modifying it, as many Linux users will tell you. But at the same time, those Linux users will not be terribly surprised or disappointed if they discover your game, that explicitly states it targets 'Ubuntu 16.04 & Up', doesn't work on their rooted ARM CPU powered smart-toaster running Raspbian.

As long as the minimum requirements are clearly stated, the community will happily accept this. Chances are, your game will run fine on all popular Linux distributions by targeting Ubuntu, and many Linux gamers will enjoy your game even if they don't use Ubuntu.

**Unofficial Linux Builds**

If you are still not confident about the stability or optimisation of the Linux version of your game, but are interested in providing it anyway, you do have the option of providing a Linux build as an unofficial build. On Steam for example, there is the option to provide a native Linux build of a game, that can be installed the same way as an official version, without indicating on the Steam store page that there is official native Linux support.

Many games already do this, such as:

* Thomas Was Alone
* Faerie Solitaire
* Thea: The Awakening
* SCP: Laboratory
* The Swindle
* Arma 3.

If you are honest with the Linux gaming community, and inform them that the Linux support is unofficial or 'experimental', they will understand and likely appreciate even unofficial support. Later on, if the Linux build is a success, and runs well, you can make the support official.
